import { PredioDTO , ManzanaDTO, UrbanizacionDTO, BarrioDTO, ViaGeneradoraDTO, ViaPrincipalDTO, DireccionEstandarDTO } from './direccion-estandar.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

const DEFAULT: DireccionEstandarDTO = { barrio: null, manzana: null, predio: null, urbanizacion: null, viaGeneradora: null, viaPrincipal: null, complemento: null, observaciones: null };

export class DireccionEstandarFormulario extends FormGroup {
  diligenciada: string;
  estandarizada: string;

  constructor(value: DireccionEstandarDTO = DEFAULT) {
    super({
      observaciones: new FormControl(value.observaciones, []),
      complemento: new FormControl(value.complemento, []),
      barrio: new FormGroup({
        tipo: new FormControl(value.barrio?.tipo, [])
      }),
      manzana: new FormGroup({
        tipo: new FormControl(value.manzana?.tipo, [])
      }),
      predio: new FormGroup({
        tipo: new FormControl(value.predio?.tipo, [])
      }),
      urbanizacion: new FormGroup({
        tipo: new FormControl(value.urbanizacion?.tipo, [])
      }),
      viaGeneradora: new FormGroup({
        nombre: new FormControl(value.viaGeneradora?.nombre, []),
        letraVia: new FormControl(value.viaGeneradora?.letraVia, []),
        sufijo: new FormControl(value.viaGeneradora?.sufijo, []),
        letraSufijo: new FormControl(value.viaGeneradora?.letraSufijo, []),
        numeroPlaca: new FormControl(value.viaGeneradora?.numeroPlaca, []),
        cuadrante: new FormControl(value.viaGeneradora?.cuadrante, [])
      }),
      viaPrincipal: new FormGroup({
        tipo: new FormControl(value.viaPrincipal?.tipo, []),
        letraVia: new FormControl(value.viaPrincipal?.letraVia, [Validators.maxLength(3)]),
        prefijo: new FormControl(value.viaPrincipal?.prefijo, []),
        letraPrefijo: new FormControl(value.viaPrincipal?.letraPrefijo, [Validators.maxLength(3)]),
        cuadrante: new FormControl(value.viaPrincipal?.cuadrante, [])
      }, Validators.required)
    });

    this.ViaPrincipal.addControl('nombreONumero', new FormControl(value.viaPrincipal?.nombreONumero, [
      Validators.maxLength(10), (control) => {
        if (this.ViaPrincipal.controls.tipo.value?.abreviatura !== 'NA') {
          return obligatioDependiente(this.ViaPrincipal.controls.tipo)(control);
        }
        return null;
      }])
    );

    this.Barrio.addControl('nombre', new FormControl(value.barrio?.nombre, [obligatioDependiente(this.Barrio.controls.tipo)]));
    this.Manzana.addControl('nombre', new FormControl(value.manzana?.nombre, [obligatioDependiente(this.Manzana.controls.tipo)]));
    this.Predio.addControl('nombre', new FormControl(value.predio?.nombre, [obligatioDependiente(this.Predio.controls.tipo)]));
    this.Urbanizacion.addControl('nombre', new FormControl(value.urbanizacion?.nombre, [obligatioDependiente(this.Urbanizacion.controls.tipo)]));

    this.ViaPrincipal.controls.tipo.valueChanges.subscribe(v => this.ViaPrincipal.controls.nombreONumero.updateValueAndValidity());
    this.Barrio.controls.tipo.valueChanges.subscribe(v => this.Barrio.controls.nombre.updateValueAndValidity());
    this.Manzana.controls.tipo.valueChanges.subscribe(v => this.Manzana.controls.nombre.updateValueAndValidity());
    this.Predio.controls.tipo.valueChanges.subscribe(v => this.Predio.controls.nombre.updateValueAndValidity());
    this.Urbanizacion.controls.tipo.valueChanges.subscribe(v => this.Urbanizacion.controls.nombre.updateValueAndValidity());

    this.valueChanges.subscribe(val => this.estandarizar(val));
  }

  get ViaPrincipal(): FormGroup {
    return this.controls.viaPrincipal as FormGroup;
  }

  get ViaGeneradora(): FormGroup {
    return this.controls.viaGeneradora as FormGroup;
  }

  get Urbanizacion(): FormGroup {
    return this.controls.urbanizacion as FormGroup;
  }

  get Predio(): FormGroup {
    return this.controls.predio as FormGroup;
  }

  get Manzana(): FormGroup {
    return this.controls.manzana as FormGroup;
  }

  get Barrio(): FormGroup {
    return this.controls.barrio as FormGroup;
  }

  get valid(): boolean {
    return this.noVacio(this.ViaPrincipal.value)
      && (this.noVacio(this.ViaGeneradora.value) || (this.noVacio(this.Manzana.value) && this.Manzana.valid))
  }

  private estandarizar(value: DireccionEstandarDTO): void {
    this.diligenciada = concatenar([
      this.formatearViaPrincipal(value.viaPrincipal, false), 
      this.formatearViaGeneradora(value.viaGeneradora, false),
      this.formatearBarrio(value.barrio, false),
      this.formatearUrbanizacion(value.urbanizacion, false),
      this.formatearManzana(value.manzana, false),
      this.formatearPredio(value.predio, false)
    ]);

    this.estandarizada = concatenar([
      this.formatearViaPrincipal(value.viaPrincipal, true), 
      this.formatearViaGeneradora(value.viaGeneradora, true),
      this.formatearBarrio(value.barrio, true),
      this.formatearUrbanizacion(value.urbanizacion, true),
      this.formatearManzana(value.manzana, true),
      this.formatearPredio(value.predio, true)
    ]);
  }

  private formatearViaPrincipal(viaPrincipal: ViaPrincipalDTO, estandarizada: boolean): string {
    const { tipo, nombreONumero, letraVia, prefijo, letraPrefijo, cuadrante } = viaPrincipal;
    return estandarizada ? concatenar([opcional(tipo?.abreviatura), `${opcional(nombreONumero)}${opcional(letraVia)}`, `${opcional(prefijo?.abreviatura)}${opcional(letraPrefijo)}`, `${opcional(cuadrante?.abreviatura)}`])
      : concatenar([opcional(tipo?.nombre), `${opcional(nombreONumero)}${opcional(letraVia)}`, `${opcional(prefijo?.nombre)}${opcional(letraPrefijo)}`, `${opcional(cuadrante?.nombre)}`]);
  }

  private formatearViaGeneradora(viaGeneradora: ViaGeneradoraDTO, estandarizada: boolean): string {
    const { nombre, letraVia, sufijo, letraSufijo, numeroPlaca, cuadrante } = viaGeneradora;
    return estandarizada ? concatenar([`${opcional(nombre)}${opcional(letraVia)}`, `${opcional(sufijo?.abreviatura)}${opcional(letraSufijo)}`, numeroPlaca ? `- ${numeroPlaca}` : '', `${opcional(cuadrante?.abreviatura)}`])
      : concatenar([`${opcional(nombre)}${opcional(letraVia)}`, `${opcional(sufijo?.nombre)}${opcional(letraSufijo)}`, numeroPlaca ? `- ${numeroPlaca}` : '', `${opcional(cuadrante?.nombre)}`]);
  }

  private formatearBarrio(barrio: BarrioDTO, estandarizada: boolean): string {
    const { nombre, tipo } = barrio;
    return estandarizada ? concatenar([`${opcional(tipo?.abreviatura)}`, opcional(nombre)]) : concatenar([`${opcional(tipo?.nombre)}`, opcional(nombre)]);
  }

  private formatearUrbanizacion(urbanizacion: UrbanizacionDTO, estandarizada: boolean): string {
    const { nombre, tipo } = urbanizacion;
    return estandarizada ? concatenar([`${opcional(tipo?.abreviatura)}`, opcional(nombre)]) : concatenar([`${opcional(tipo?.nombre)}`, opcional(nombre)]);
  }

  private formatearManzana(manzana: ManzanaDTO, estandarizada: boolean): string {
    const { nombre, tipo } = manzana;
    return estandarizada ? concatenar([`${opcional(tipo?.abreviatura)}`, opcional(nombre)]) : concatenar([`${opcional(tipo?.nombre)}`, opcional(nombre)]);
  }

  private formatearPredio(predio: PredioDTO, estandarizada: boolean): string {
    const { nombre, tipo } = predio;
    return estandarizada ? concatenar([`${opcional(tipo?.abreviatura)}`, opcional(nombre)]) : concatenar([`${opcional(tipo?.nombre)}`, opcional(nombre)]);
  }

  private noVacio(obj: any): boolean {
    return Object.keys(obj).some(key => !!obj[key])
  }
}

function concatenar(elementos: Array<string>): string {
  return elementos.filter(elemento => !!elemento).join(' ');
}

function opcional(value: string): string {
  return `${value || ''}`
}

function obligatioDependiente(dependencia) {
  return (control) => {
    if (dependencia.value?.abreviatura) {
      return Validators.required(control);
    }
    return null;
  }
}