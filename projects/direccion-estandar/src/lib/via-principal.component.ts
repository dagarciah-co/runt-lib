import { Component, Inject, InjectionToken, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Repositorio, repositorioFactoryProvider, NombreAbreviatura, compararAbreviaturas } from './direccion-estandar.model';
import { TipoVia, Prefijo } from './via-principal.model';
import { Cuadrante } from './cuadrante.model';

const RepositorioTipoVia = new InjectionToken<Repositorio<TipoVia>>('TipoVia');
const RepositorioPrefijo = new InjectionToken<Repositorio<Prefijo>>('Prefijo');
const RepositorioCuadrante = new InjectionToken<Repositorio<Cuadrante>>('Cuadrante');

@Component({
    selector: 'direccion-estandar-via-principal',
    templateUrl: `./via-principal.component.html`,
    providers: [
        { provide: RepositorioTipoVia, useFactory: repositorioFactoryProvider(TipoVia) },
        { provide: RepositorioPrefijo, useFactory: repositorioFactoryProvider(Prefijo) },
        { provide: RepositorioCuadrante, useFactory: repositorioFactoryProvider(Cuadrante) }
    ]
})
export class ViaPrincipalComponent implements OnInit {

    @Input() formulario: FormGroup;

    constructor(
        @Inject(RepositorioTipoVia) private tiposVia: Repositorio<TipoVia>, 
        @Inject(RepositorioPrefijo) private prefijos: Repositorio<Prefijo>, 
        @Inject(RepositorioCuadrante) private cuadrantes: Repositorio<Cuadrante>) { }

    ngOnInit(): void {
    }

    get TiposVia(): Array<TipoVia> {
        return this.tiposVia.all();
    }

    get Prefijos(): Array<Prefijo> {
        return this.prefijos.all();
    }

    get Cuadrantes(): Array<Cuadrante> {
        return this.cuadrantes.all();
    }

    comparador(a: NombreAbreviatura, b: NombreAbreviatura): boolean {
        return compararAbreviaturas(a, b);
    }

}