import { DireccionEstandarFormulario } from './direccion-estandar.formulario';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'runt-direccion-estandar',
  templateUrl: `./direccion-estandar.component.html`,
  styles: [`
    :host ::ng-deep label {
      font-size: 10px;
    }
    
    :host ::ng-deep strong {
      font-size: 11px;
    }

    :host ::ng-deep .bordered {
      border: 1px solid;
      padding: 2px;
      margin-bottom: 1em;
    }

    :host ::ng-deep .margen {
      margin-left: 5px;
      margin-right: 5px;
    }

    :host ::ng-deep .fontSize11 {
      font-size: 11px !important;
    }
  `]
})
export class DireccionEstandarComponent implements OnInit {

  @Input() formulario: DireccionEstandarFormulario;
  
  constructor() { }

  ngOnInit(): void {
    if (!this.formulario) {
      this.formulario = new DireccionEstandarFormulario();
    }
  }

}
