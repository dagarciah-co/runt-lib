import { faExclamation, faList } from '@fortawesome/free-solid-svg-icons';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DireccionEstandarModal } from './direccion-estandar.modal';
import { RuntDirectivasModule } from './../../../runt-directivas/src/lib/runt-directivas.module';
import { UrbanizacionComponent } from './urbanizacion.component';
import { ManzanaComponent } from './manzana.component';
import { BarrioComponent } from './barrio.component';
import { ViaPrincipalComponent } from './via-principal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { DireccionEstandarComponent } from './direccion-estandar.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ViaGeneradoraComponent } from './via-generadora.component';
import { PredioComponent } from './predio.component';
import { ComplementoComponent } from './complemento.component';

let fixture: ComponentFixture<DireccionEstandarComponent> = null;
let component: DireccionEstandarComponent = null;

describe('DireccionEstandarComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        PredioComponent,
        BarrioComponent,
        ManzanaComponent,
        ComplementoComponent,
        UrbanizacionComponent,
        ViaPrincipalComponent,
        ViaGeneradoraComponent,
        DireccionEstandarComponent
      ],
      imports: [
        NgbModule,
        NgSelectModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        RuntDirectivasModule
      ]
    }).compileComponents();

    const library = TestBed.inject(FaIconLibrary);
    library.addIcons(faExclamation, faList);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DireccionEstandarComponent);
    component = fixture.debugElement.componentInstance;

    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid', () => {
    const formulario = component.formulario.ViaPrincipal;

    formulario.controls.tipo.setValue('AU');
    formulario.controls.nombreONumero.updateValueAndValidity();    

    fixture.detectChanges();

    expect(formulario.valid).toBeFalsy();

    new DireccionEstandarModal(TestBed.inject(NgbModal)).open((x, z) => console.log(x));
    
    fixture.detectChanges();    
  });
})  