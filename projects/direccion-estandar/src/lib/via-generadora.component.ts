import { Component, Inject, InjectionToken, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Repositorio, repositorioFactoryProvider, NombreAbreviatura, compararAbreviaturas } from './direccion-estandar.model';
import { Cuadrante } from './cuadrante.model';
import { Sufijo } from './via-generadora.model';

const RepositorioSufijo = new InjectionToken<Repositorio<Sufijo>>('Sufijo');
const RepositorioCuadrante = new InjectionToken<Repositorio<Cuadrante>>('Cuadrante');

@Component({
    selector: 'direccion-estandar-via-generadora',
    templateUrl: `./via-generadora.component.html`,
    providers: [
        { provide: RepositorioSufijo, useFactory: repositorioFactoryProvider(Sufijo) },
        { provide: RepositorioCuadrante, useFactory: repositorioFactoryProvider(Cuadrante) }
    ]
})
export class ViaGeneradoraComponent implements OnInit {

    @Input() formulario: FormGroup;

    constructor(
        @Inject(RepositorioCuadrante)private cuadrantes: Repositorio<Cuadrante>, 
        @Inject(RepositorioSufijo)private sufijos: Repositorio<Sufijo>) { }

    ngOnInit(): void {

    }

    get Sufijos(): Array<Sufijo> {
        return this.sufijos.all()
    }

    get Cuadrantes(): Array<Cuadrante> {
        return this.cuadrantes.all();
    }

    comparador(a: NombreAbreviatura, b: NombreAbreviatura): boolean {
        return compararAbreviaturas(a, b);
    }
}