import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DireccionEstandarModalContentComponent } from './direccion-estandar-modal-content.component';


@Injectable()
export class DireccionEstandarModal {
    constructor(private modalService: NgbModal) { }

    open(onCerrar: (razon: ModalDismissReasons | string | null, esRechazo: boolean) => void): NgbModalRef {
        const modalRef: NgbModalRef = this.modalService.open(DireccionEstandarModalContentComponent, {size: 'xl', ariaLabelledBy: 'modal-basic-title' });
        modalRef.result.then((result) => {
            onCerrar(result, false);
        }, (reason) => {
            onCerrar(reason, true);
        });

        return modalRef;
    }
}
