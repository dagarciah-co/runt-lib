import { NombreAbreviatura, Repositorio } from './direccion-estandar.model';

export class Cuadrante extends NombreAbreviatura {
}

Repositorio.agregar(Cuadrante, [
    {
        nombre: 'Este',
        abreviatura: 'ESTE'
    },
    {
        nombre: 'Norte',
        abreviatura: 'NORTE'
    },
    {
        nombre: 'Oeste',
        abreviatura: 'OESTE'
    },
    {
        nombre: 'Sur',
        abreviatura: 'SUR'
    }
]);