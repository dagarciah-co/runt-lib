import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { repositorioFactoryProvider, compararAbreviaturas, Repositorio } from './direccion-estandar.model';
import { Barrio } from './barrio.model';

@Component({
    selector: 'direccion-estandar-barrio',
    templateUrl: `./barrio.component.html`,
    providers: [
        { provide: Repositorio, useFactory: repositorioFactoryProvider(Barrio) }
    ]
})
export class BarrioComponent implements OnInit {

    @Input() formulario: FormGroup;

    constructor(private repositorio: Repositorio<Barrio>) { }

    ngOnInit(): void {
    }

    get Barrios(): Array<Barrio> {
        return this.repositorio.all();
    }

    comparador(a: Barrio, b: Barrio):  boolean {
        return compararAbreviaturas(a, b);
    }
}