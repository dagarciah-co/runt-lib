const almacenamiento = {}

export class NombreAbreviatura {
  constructor(readonly abreviatura: string = null, readonly nombre?: string) { }
}

export class Repositorio<T extends NombreAbreviatura> {
  private nombre: string;
  constructor(x: new () => T) {
    this.nombre = x.name;
  }

  all(): Array<T> {
    return almacenamiento[this.nombre];
  }

  findById(id: string): T {
    return (almacenamiento[this.nombre] || {})[id];
  }

  static agregar<T extends NombreAbreviatura>(x: new () => T, valor: Array<T>) {
    almacenamiento[x.name] = valor;
  }
}

export function compararAbreviaturas<T extends NombreAbreviatura>(a: T, b: T): boolean {
  return (!!a && !!b) && (a.abreviatura == b.abreviatura);
}

export function repositorioFactoryProvider<T extends NombreAbreviatura>(x: new () => T): () => Repositorio<T> {
  return () => new Repositorio<T>(x);
}

export interface ManzanaDTO {
  tipo?: NombreAbreviatura;
  nombre?: string;
}

export interface BarrioDTO {
  tipo?: NombreAbreviatura;
  nombre?: string;
}

export interface PredioDTO {
  tipo?: NombreAbreviatura;
  nombre?: string;
}

export interface UrbanizacionDTO {
  tipo?: NombreAbreviatura;
  nombre?: string;
}

export interface ViaGeneradoraDTO {
  nombre?: string;
  letraVia?: string;
  sufijo?: NombreAbreviatura;
  letraSufijo?: string;
  numeroPlaca?: string;
  cuadrante?: NombreAbreviatura;
}

export interface ViaPrincipalDTO {
  tipo?: NombreAbreviatura;
  nombreONumero?: string;
  letraVia?: string;
  prefijo?: NombreAbreviatura;
  letraPrefijo?: string;
  cuadrante?: NombreAbreviatura;
}

export interface DireccionEstandarDTO {
  barrio?: BarrioDTO;
  manzana?: ManzanaDTO;
  predio?: PredioDTO;
  urbanizacion?: UrbanizacionDTO;
  viaGeneradora?: ViaGeneradoraDTO;
  viaPrincipal: ViaPrincipalDTO;
  complemento?: string;
  observaciones?: string;
}

