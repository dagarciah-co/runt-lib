import { BrowserModule } from '@angular/platform-browser';
import { DireccionEstandarAyudaComponent } from './direccion-estandar-ayuda.component';
import { DireccionEstandarModalContentComponent } from './direccion-estandar-modal-content.component';
import { DireccionEstandarModal } from './direccion-estandar.modal';
import { RuntDirectivasModule } from 'runt-directivas';
import { UrbanizacionComponent } from './urbanizacion.component';
import { ViaPrincipalComponent } from './via-principal.component';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DireccionEstandarComponent } from './direccion-estandar.component';
import { ViaGeneradoraComponent } from './via-generadora.component';
import { BarrioComponent } from './barrio.component';
import { ManzanaComponent } from './manzana.component';
import { PredioComponent } from './predio.component';
import { ComplementoComponent } from './complemento.component';
import { faExclamation, faList } from '@fortawesome/free-solid-svg-icons';


@NgModule({
  declarations: [
    PredioComponent,
    BarrioComponent,
    ManzanaComponent,
    ComplementoComponent,
    UrbanizacionComponent,
    ViaPrincipalComponent,
    ViaGeneradoraComponent,
    DireccionEstandarComponent,
    DireccionEstandarAyudaComponent,
    DireccionEstandarModalContentComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    NgSelectModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    NgbModule,
    RuntDirectivasModule
  ],
  exports: [
    DireccionEstandarComponent,
    DireccionEstandarAyudaComponent,
    DireccionEstandarModalContentComponent
  ],
  providers: [DireccionEstandarModal]
})
export class DireccionEstandarModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faExclamation, faList);
  }
}
