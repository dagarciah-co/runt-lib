import { DireccionEstandarModule } from 'direccion-estandar';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppComponent } from './app.component';
import { RuntDirectivasModule } from 'runt-directivas';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    NgbModule,
    NgSelectModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    RuntDirectivasModule,
    DireccionEstandarModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
