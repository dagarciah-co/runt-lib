import { DireccionEstandarModal } from 'direccion-estandar';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'demo';

  constructor(private modal: DireccionEstandarModal) {}

  onClick(): void {
    this.modal.open((x, y) => console.log(x, y));
  }
}
