import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[OnlyLetters]'
})
export class OnlyLettersDirective {

    key;

    constructor(private el: ElementRef) { }

    @Input() OnlyLetters: boolean;

    @HostListener('keydown', ['$event'])
    onKeyDown(event) {
        let e = <KeyboardEvent>event;
        if (this.OnlyLetters) {
            this.key = e.keyCode;
            if ((this.key >= 15 && this.key <= 64) || (this.key >= 123) || (this.key >= 96 && this.key <= 105)) {
                event.preventDefault();
            }
        }
    }
}