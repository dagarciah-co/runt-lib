import { UppercaseInputDirective } from './uppercase.directive';
import { HasErrorDirective } from './has-error.directive';
import { OnlyLettersDirective } from './letters-only.directive';
import { OnlyNumberDirective } from './number-only.directive';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    OnlyNumberDirective,
    OnlyLettersDirective,
    HasErrorDirective,
    UppercaseInputDirective
  ],
  imports: [
  ],
  exports: [
    OnlyNumberDirective,
    OnlyLettersDirective,
    HasErrorDirective,
    UppercaseInputDirective
  ]
})
export class RuntDirectivasModule { }
