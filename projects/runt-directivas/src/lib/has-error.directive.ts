import { DefaultMessageFnResolver } from './mensaje-fn.resolver';
import { Directive, ElementRef, OnChanges, OnInit, Renderer2, Input } from '@angular/core';
import { NgControl, ValidationErrors } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MessageFnResolver, MessageFn } from './mensaje-fn.model';


@Directive({
    selector: '[HasError]',
    providers: [{ provide: DefaultMessageFnResolver }]
})
export class HasErrorDirective implements OnInit, OnChanges {
    private subscripcion: Subscription;
    private customFnResolver: MessageFnResolver;
    @Input("submitted") submitted: boolean;
    @Input("HasError") contenedor: HTMLElement;
    @Input("mensajes") set Mensajes(mensajes: { [key: string]: MessageFn }) {
        this.customFnResolver = new CustomMessageFnResolver(this.defaultFnResolver, mensajes);
    }

    constructor(
        private defaultFnResolver: DefaultMessageFnResolver,
        private renderer: Renderer2,
        private el: ElementRef,
        private control: NgControl) {

        this.customFnResolver = new CustomMessageFnResolver(this.defaultFnResolver, {});
    }

    ngOnInit(): void {
        this.subscripcion = this.control.statusChanges
            .subscribe(estado => this.actualizarErrores(estado));
    }

    ngOnChanges(change): void {
        if (change.submitted) {
            this.submitted = change.submitted.currentValue;
            this.actualizarErrores(this.control.status)
        }
    }

    OnDestroy() {
        this.subscripcion.unsubscribe();
    }

    /**
     * Agrega o elimina los errores al camponente 
     * 
     * @param estado :string - Estado actual del componente
     */
    private actualizarErrores(estado: string) {
        let classList = this.el.nativeElement.classList;
        let hasClass = classList.contains('form-control') || classList.contains('form-input');
        let elements = (hasClass ? [this.el.nativeElement] : [])
            .concat(Array.from(this.encontrarPorClaseCss('form-input')))
            .concat(Array.from(this.encontrarPorClaseCss('form-control')));
        for (let i = 0; i < elements.length; i++) {
            let element = elements[i];
            if (estado === 'INVALID' && this.submitted) {
                this.renderer.addClass(element, 'is-invalid');
                this.agregarMensajes(this.control.errors);
            } else {
                this.renderer.removeClass(element, 'is-invalid');
                this.eliminarMensajes();
            }
        }
    }

    /**
     * Elimina los mensajes que contenga el contenedor
     */
    private eliminarMensajes() {
        if (this.contenedor) {
            Array.from(this.contenedor.children)
                .forEach(child => this.contenedor.removeChild(child));
        }
    }

    /**
     * Crea tantos errores como validaciones fallidas tenga el componente 
     * 
     * @param errors :ValidationErrors - Un key:value con los errores 
     * identificador por el fomulario reactivo
     */
    private agregarMensajes(errors: ValidationErrors) {
        this.eliminarMensajes();
        if (errors && this.contenedor) {
            Object.keys(errors)
                .map(key => this.customFnResolver.of(key)(errors[key]))
                .forEach(mensaje => {
                    let error = document.createElement(`span`);
                    error.setAttribute('class', 'text-danger clearfix')
                    error.textContent = mensaje;

                    this.contenedor.appendChild(error);
                });
        }
    }


    private encontrarPorClaseCss(clase): HTMLCollection {
        return this.el.nativeElement.getElementsByClassName(clase);
    }
}

class CustomMessageFnResolver implements MessageFnResolver {
    constructor(private defaultFnResolver: MessageFnResolver, private messages: { [key: string]: MessageFn }) { }

    of(key: string): MessageFn {
        return this.messages[key] ? this.messages[key] : this.defaultFnResolver.of(key);
    }

}