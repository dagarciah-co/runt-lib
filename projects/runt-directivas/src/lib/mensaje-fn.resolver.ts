import { Injectable } from '@angular/core';
import { MessageFn, MessageFnResolver } from './mensaje-fn.model';

@Injectable()
export class DefaultMessageFnResolver implements MessageFnResolver {
    private mensajes: { [key: string]: MessageFn } = {
        required: () => 'El campo es obligatorio.',
        max: (error) => `El valor m\u00e1ximo permitido es ${error.max}.`,
        min: (error) => `El valor m\u00ednimo permitido es ${error.min}.`,
        maxlength: (error) => `La longitud m\u00e1xima es ${error.requiredLength} caracteres.`,
        minlength: (error) => `La longitud m\u00ednima es ${error.requiredLength} caracteres.`,
        email: () => `El campo no contiene un formato de correo electr\u00f3nico valido.`,
        maxSize: (error) => `El archivo seleccionado excede el tama\u00f1o de ${error.maxSize} Mega${error.maxSize > 1 ? 's' : ''}.`,
        maxLengthArray: (error) => `No se permiten cargar m\u00e1s de ${error.maxLengthArray} registros.`,
        pattern: (error) => 'El campo contiene carácteres inválidos',
        horaFinMenor: (error) => 'La hora fin debe ser mayor a la hora inicio',
        horaFinMayor: (error) => 'La hora inicio debe ser menor a la hora fin'
    }

    of(key: string): MessageFn {
        return this.mensajes[key] || (() => key)
    }
}

